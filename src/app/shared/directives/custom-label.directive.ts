import { Directive, ElementRef, Input } from '@angular/core';
import { ValidationErrors } from '@angular/forms';

@Directive({
  selector: '[CustomLabel]'
})
export class CustomLabelDirective {

  private htmlElement?:ElementRef<HTMLElement>;
  private _color:string='red'
  private _errors:ValidationErrors | null | undefined;

  @Input() set color ( value:string){
    console.log({color:value});
    this._color=value;
    this.setStyle();
  }

  @Input() set errors (value:ValidationErrors | null | undefined){
    this._errors=value;
    console.log(value);
    this.setErrors();
  }

  constructor(private el:ElementRef<HTMLElement>) { 
    console.log(el);
    this.htmlElement=el;
  }

  setStyle(){
    if(!this.htmlElement) return;
    this.htmlElement!.nativeElement.style.color = this._color;
  }

  setErrors():void{
    if(!this.htmlElement) return;
    if(!this._errors){
      this.htmlElement.nativeElement.innerText='';
      return;
    }
    const errors = Object.keys(this._errors);
    // console.log(errors);
    if(errors.includes('required')){
      this.htmlElement.nativeElement.innerText='Este campo es requerido';
      return;
    }
    if(errors.includes('email')){
      this.htmlElement.nativeElement.innerText='Este campo debe ser un email valido';
      return;
    }
    if(errors.includes('minlength')){
      const min = this._errors['minlength']['requiredLength'];
      const current= this._errors['minlength']['actualLength'];
      this.htmlElement.nativeElement.innerText=`Este campo debe tener ${min}/${current} caracteres`;
      return;
    }
  }

}
