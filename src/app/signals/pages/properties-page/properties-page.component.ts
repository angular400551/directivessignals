import { Component, effect, signal } from '@angular/core';
import { User } from '../../interfaces/user-request.interface';

@Component({
  templateUrl: './properties-page.component.html',
  styleUrl: './properties-page.component.css'
})
export class PropertiesPageComponent {

  public userChangedEfect = effect(()=>{
    console.log(this.user().first_name);
  })

  public user= signal<User>({
    id: 1,
    email: "george.bluth@reqres.in",
    first_name: "George",
    last_name: "Bluth",
    avatar: "https://reqres.in/img/faces/1-image.jpg"
  })

  onFieldUpdated(field:string, value:string){
    console.log({field,value});
    this.user.update((current)=>{
      switch(field){
        case 'email':
          current.email=value;
          break;  
        case 'firstName':
          current.first_name=value;
          break;  
        case 'lastName':
          current.last_name=value;
          break;  
        case 'id':
          current.id=Number(value);
          break;  
        case 'avatar':
          current.avatar=value;
          break;  

        
  
      }
      return current;
    })
  }
}
